package app.com.getinbouncer.Utils

import android.content.Context
import android.content.SharedPreferences

class GetInBouncerSharedPrefrences {

    private var sharedPrefrences:SharedPreferences
    private var prefEditor:SharedPreferences.Editor

    private constructor(context:Context)
    {
        sharedPrefrences=context.getSharedPreferences(Constants.sharedPrefrenceName,Context.MODE_PRIVATE)
        prefEditor=sharedPrefrences.edit()
    }

    companion object {
        private var INSTANCE :GetInBouncerSharedPrefrences?=null

        fun getInBouncerSharedPrefrencesInstance(context: Context): GetInBouncerSharedPrefrences{
            if (INSTANCE==null){
                INSTANCE= GetInBouncerSharedPrefrences(context)
            }
            return INSTANCE as GetInBouncerSharedPrefrences
        }
    }


    fun getUserLoginStatus() : Boolean {
        return sharedPrefrences.getBoolean("userLoginStatus",false)
    }



    fun clearSharedPrefrences(){
        prefEditor.clear()
        prefEditor.commit()
    }

}