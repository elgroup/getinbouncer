package app.com.getinbouncer.Adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter

class PagerAdapter(supportFragmentManager: FragmentManager?) : FragmentPagerAdapter(supportFragmentManager) {
    private var mFragmentList=ArrayList<Fragment>()

    override fun getItem(pos: Int): Fragment {
     return  mFragmentList.get(pos)
    }

    override fun getCount(): Int {
      return  mFragmentList.size
    }

    fun addFragment(fragment:Fragment){
        mFragmentList.add(fragment)
    }
}
