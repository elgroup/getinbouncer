package app.com.getinbouncer.Activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import app.com.getinbouncer.R

class SignInActivity : BaseActivity(), View.OnClickListener {
    override fun onClick(view: View?) {
        if (view==buy_ticket){
            startActivity(Intent(this@SignInActivity,DashBoardActivity::class.java))
        }
    }

    lateinit var buy_ticket:Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        init()
    }

    private fun init() {
        buy_ticket=findViewById(R.id.buy_ticket)
        buy_ticket.setOnClickListener(this)

    }
}
