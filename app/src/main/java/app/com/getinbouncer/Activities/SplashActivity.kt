package app.com.getinbouncer.Activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import app.com.getinbouncer.R

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed(object:Runnable{
            override fun run() {
                if (getInBouncerSharedPrefrences.getUserLoginStatus()) {
                    // OPEN DASHBOARD PAge

                } else {
                    // OPEN LOGIN PAGE
                    val i = Intent(this@SplashActivity, SignInActivity::class.java)
                    startActivity(i)

                }
            }

        },2000)
    }
}
