package app.com.getinbouncer.Activities

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import app.com.getinbouncer.Utils.GetInBouncerSharedPrefrences

open class BaseActivity : AppCompatActivity() {

     lateinit var getInBouncerSharedPrefrences:GetInBouncerSharedPrefrences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getInBouncerSharedPrefrences= GetInBouncerSharedPrefrences.getInBouncerSharedPrefrencesInstance(this)
    }
}