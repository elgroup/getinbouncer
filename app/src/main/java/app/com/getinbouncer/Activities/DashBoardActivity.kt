package app.com.getinbouncer.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import app.com.getinbouncer.Adapters.PagerAdapter
import app.com.getinbouncer.Fragments.DashboardFragment
import app.com.getinbouncer.Fragments.ProfileFragment
import app.com.getinbouncer.Fragments.ScannerFragment
import app.com.getinbouncer.R

class DashBoardActivity : BaseActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v){
            tab_home->{
                HomeTabSelected()
            }
            tab_scan->{
                ScanTabSelected()
            }
            tab_profile->{
                ProfileTabSelected()
            }
        }
    }

    //viewpager
    lateinit var mView_pager:ViewPager

    //bottom tab layouts
    lateinit var tab_home:LinearLayout
    lateinit var tab_scan:LinearLayout
    lateinit var tab_profile:LinearLayout
    //bottom tab views
    lateinit var home_view:ImageView
    lateinit var scan_view:ImageView
    lateinit var profile_view:ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dash_board)
   //to initialized all members
      init()
    }

    private fun init() {
        mView_pager=findViewById(R.id.mView_pager)

//getting views id for tab layout
        tab_home=findViewById(R.id.tab_home)
        tab_scan=findViewById(R.id.tab_scan)
        tab_profile=findViewById(R.id.tab_profile)
        home_view=findViewById(R.id.home_view)
        scan_view=findViewById(R.id.scan_view)
        profile_view=findViewById(R.id.profile_view)

        //click listeners
        tab_home.setOnClickListener(this)
        tab_scan.setOnClickListener(this)
        tab_profile.setOnClickListener(this)

      // creating adaoter for view pager
        val pagerAdapter = PagerAdapter(supportFragmentManager)

        // addding fragments in viewpager
        pagerAdapter.addFragment(DashboardFragment())
        pagerAdapter.addFragment(ScannerFragment())
        pagerAdapter.addFragment(ProfileFragment())

        // setting adapter in view pader
        mView_pager.adapter = pagerAdapter


        // page listerner for viewpager
        mView_pager.addOnPageChangeListener(object :ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(pos: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(pos: Int) {
                when(pos){
                    0->HomeTabSelected()
                    1->ScanTabSelected()
                    2->ProfileTabSelected()
                }

            }

        })

        //first time call
        HomeTabSelected()
    }

    fun HomeTabSelected(){
        home_view.setImageResource(R.drawable.home_selected)
        scan_view.setImageResource(R.drawable.qr_code)
        profile_view .setImageResource(R.drawable.profile)
        mView_pager.setCurrentItem(0)
    }

    fun ScanTabSelected(){

        home_view.setImageResource(R.drawable.home)
        scan_view.setImageResource(R.drawable.qr_code_selected)
        profile_view .setImageResource(R.drawable.profile)
        mView_pager.setCurrentItem(1)
    }

    fun ProfileTabSelected(){

        home_view.setImageResource(R.drawable.home)
        scan_view.setImageResource(R.drawable.qr_code)
        profile_view .setImageResource(R.drawable.profile_selected)
        mView_pager.setCurrentItem(2)
    }

}
